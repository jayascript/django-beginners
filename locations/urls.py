from django.urls import path
from locations.views import HomePage


urlpatterns = [
    path('', HomePage.as_view(), name="home"),
]
