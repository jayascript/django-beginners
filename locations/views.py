from django.views.generic import ListView
from locations.models import City

class HomePage(ListView):
    model = City
    template_name = "home.html"

